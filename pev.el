;;; pev.el --- Edit POM version                      -*- lexical-binding: t; -*-

;; Copyright (C) 2014  Ian Eure

;; Author: Ian Eure <ian.eure@gmail.com>
;; Keywords: tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(defvar pev-project-root nil "Root of the current project")
(make-variable-buffer-local 'pev-project-root)

(defvar pev-pom-name nil "The current POM version")
(make-variable-buffer-local 'pev-pom-name)

(defvar pev-pom-version nil "The current POM name")
(make-variable-buffer-local 'pev-pom-version)

(defun pev-locate-root-pom (&optional base)
  (unless (or base (buffer-file-name))
    (error "No file associated with buffer"))

  (let ((prev nil)
        (root (or base default-directory)))
    (while root
      (setq prev root
            root (locate-dominating-file
                  (file-name-directory (substring root 0 -1)) "pom.xml")))
    (let ((file (format "%spom.xml" prev)))
      (and (file-exists-p file) file))))

(defun pev-get-pom-name (&optional pom)
  (let ((pom (or pom (xml-parse-file (pev-locate-root-pom)))))
    (caddr (assoc 'name (cdr (assoc 'project pom))))))

(defun pev-get-pom-version (&optional pom)
  (let ((pom (or pom (xml-parse-file (pev-locate-root-pom)))))
    (caddr (assoc 'version (cdr (assoc 'project pom))))))

(defun pev-edit-version ()
  (interactive)
  (let* ((pom-file (pev-locate-root-pom))
         (pom (xml-parse-file pom-file))
         (v (pev-get-pom-version pom))
         (n (pev-get-pom-name pom))
         (buf (pop-to-buffer (format "*PEV:edit %s*" n))))
    (pev-edit-version-mode)
     ;; pev-project-root (file-name-directory pom-file))
     (setq
      default-directory (file-name-directory pom-file)
      pev-pom-name n
      pev-pom-version v)
    (widen)
    (delete-region (point-min) (point-max))
    (insert v)))



(defun pev-read-version ()
  "Read the new version from a PEV version edit buffer"
  (trim (buffer-substring-no-properties (point-min) (point-max))))

(defun pev-move-versions (&optional n)
  (save-match-data
    (search-forward "." (line-end-position) t n)))

(defun pev-increment-patch ()
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (when (pev-move-versions 2)
      (pev-increment))))

(defun pev-increment-minor ()
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (when (pev-move-versions 1)
      (pev-increment)
      (when (pev-move-versions 1)
        (pev-replace 0)))))

(defun pev-increment-major ()
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (pev-increment)
    (when (pev-move-versions 1)
      (pev-replace 0)
      (when (pev-move-versions 1)
        (pev-replace 0))))
  )

(defun pev-increment ()
  (let* ((p (bounds-of-thing-at-point 'word))
         (s (car p))
         (e (cdr p))
         (nv (+ 1 (thing-at-point 'number))))
    (goto-char s)
    (delete-region s e)
    (insert (int-to-string nv))))

(defun pev-replace (nv)
  (let* ((p (bounds-of-thing-at-point 'word))
         (s (car p))
         (e (cdr p)))
    (goto-char s)
    (delete-region s e)
    (insert (int-to-string nv))))

(defun pev-snapshot ()
  (interactive)
  (save-excursion
    (save-match-data
      (goto-char (point-min))
      (goto-char (line-end-position))
      (unless (looking-back "-SNAPSHOT.*") (line-beginning-position)
              (insert "-SNAPSHOT")))))

(defun pev-release ()
  (interactive)
    (save-excursion
      (save-match-data
        (goto-char (point-min))
        (search-forward "-SNAPSHOT" (line-end-position) t)
        (delete-region (match-beginning 0) (line-end-position))
        (pev-increment-patch))))

(defun pev-save ()
  (interactive)
  (let ((b (current-buffer)))
    (pev-set-version (pev-read-version))
    (kill-buffer b)))

(defun pev-set-version (version)
  (let ((buf (pop-to-buffer (format "*PEV:set %s*" pev-pom-name))))
    (delete-region (point-min) (point-max))
    (start-process-shell-command "pev-set" buf
                   (format "mvn -q versions:set -DnewVersion=%s" version))))

(defvar pev-edit-version-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "\C-c\C-c" 'pev-save)
    (define-key map "\C-c\C-p" 'pev-increment-patch)
    (define-key map "\C-c\C-m" 'pev-increment-minor)
    (define-key map "\C-c\C-j" 'pev-increment-major)
    (define-key map "\C-c\C-r" 'pev-release)
    (define-key map "\C-c\C-s" 'pev-snapshot)
    map))

(define-derived-mode pev-edit-version-mode fundamental-mode
  "POM Edit Version"
  "Major mode for editing POM versions.

\\{yaml-mode-map}")

(provide 'pev)
;;; pev.el ends here
